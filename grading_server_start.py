from grading_server.app import app_


if __name__ == '__main__':
    app_.run(host='127.0.0.1', port=8080, debug=True)
