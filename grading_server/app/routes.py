from . import app_
from flask import request, jsonify
import json
from predictions import modelA, modelC
from model_b import ModelB


@app_.route('/predict', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data_json = json.loads(request.data)
        modelId = data_json['modelId']
        if modelId == 'A':
            score = modelA(data_json['question'], data_json['questionResponse'])
            data = {'score': score}
            return jsonify(data)
        elif modelId == 'B':
            filename = 'Weightless_dataset_train.csv'
            model_b = ModelB(filename)
            score = model_b.predict_final_rating(data_json['question'], data_json['questionResponse'])
            data = {'score': score}
            return jsonify(data)
        elif modelId == 'C':
            score = modelC(data_json['question'], data_json['questionResponse'])
            data = {'score': score}
            return jsonify(data)
        else:
            pass

        return 'OK'
    else:
        return error('Method must be post')


def error(str_msg):
    response = """ 
            <!DOCTYPE html>
                <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>Error</title>
                    </head>
                    <body>
                        <h1>""" + str_msg + """</h1>
                    </body>
                </html>"""
    return response


