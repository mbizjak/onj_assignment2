### 2nd assignment ###
MapBooks - Automatic Question Response Grading


## Installation

Use *pip* to install required packages:

```bash
pip install -r requirements.txt
```

Optionally download brown corpus if you want to use model C with cosine similarity (by default model uses Wu-Palmer similarity from WordNet).


## How to use?

To run evaluation server, run the *grading_server_start.py* script:

```bash
python grading_server_start.py
```

Server will start on http://localhost:8080.

To make predictions make POST request to http://localhost:8080/predict.

### Contributors ###
* **Denis Rajković**
* **Matic Bizjak**