import pandas as pd
from collections import Counter
import sys


def text_to_words(text):
    # .replace('\'', ' ')\ probi se to
    return text \
        .replace('-', '') \
        .replace('"', '') \
        .replace('!', '') \
        .replace('.', '') \
        .replace(',', '') \
        .replace('\'', '') \
        .replace('\\', '') \
        .replace('/', '') \
        .replace('(', '') \
        .replace(')', '') \
        .lower() \
        .split()


def levenshtein_dinstance(word_a, word_b):
    rows = len(word_a) + 1
    cols = len(word_b) + 1
    distances = [[0 for _ in range(cols)] for _ in range(rows)]

    for i in range(1, rows):
        distances[i][0] = i

    for i in range(1, cols):
        distances[0][i] = i

    for col in range(1, cols):
        for row in range(1, rows):
            if word_a[row - 1] == word_b[col - 1]:
                cost = 0
            else:
                cost = 1
            distances[row][col] = min(distances[row - 1][col] + 1,  # deletion
                                      distances[row][col - 1] + 1,  # insertion
                                      distances[row - 1][col - 1] + cost)  # substitution

    return distances[len(word_a)][len(word_b)]


def text_distance(text_a, text_b):
    text_a_words = text_to_words(text_a)
    text_b_words = text_to_words(text_b)

    distance_sum = 0
    for word_a in text_a_words:
        min_word_distance = sys.maxsize
        for word_b in text_b_words:
            word_distance = levenshtein_dinstance(word_a, word_b)
            if word_distance < min_word_distance:
                min_word_distance = word_distance
        distance_sum += min_word_distance
    return distance_sum


class ModelB:
    def __init__(self, file_path):
        question_col_index = 10
        response_col_index = 11
        glenns_rating_col_index = 12
        ambers_rating_col_index = 13
        final_rating_col_index = 14
        file_cols = [question_col_index, response_col_index, glenns_rating_col_index, ambers_rating_col_index, final_rating_col_index]
        self.file_content = pd.read_csv(file_path, encoding='utf-8', usecols=file_cols)

    def get_best_response_match(self, question, response, final_rating):
        question = question.strip()
        response = response.strip()
        responses = []

        for index, row in self.file_content.iterrows():
            q = row['Question'].strip()
            r = row['Response'].strip()
            fr = float(row['Final.rating'].strip().replace(',', '.'))

            if q == question and fr == final_rating:  #and r != response:
                responses.append(r)

        best_response_match = ''
        best_response_match_distance = sys.maxsize
        best_response_match_final_rating = 0.0

        for i, r in enumerate(responses):
            response_distance = text_distance(response, r)
            if response_distance < best_response_match_distance:
                best_response_match = r
                best_response_match_distance = response_distance
                best_response_match_final_rating = final_rating

        return [best_response_match, best_response_match_distance, best_response_match_final_rating]


    def predict_final_rating(self, question, response):
        best_response_match_a = self.get_best_response_match(question, response, 1.0)
        best_response_match_b = self.get_best_response_match(question, response, 0.5)
        best_response_match_c = self.get_best_response_match(question, response, 0.0)

        min_rd = min(best_response_match_a[1], best_response_match_b[1], best_response_match_c[1])
        same_distance_responses = []

        if min_rd == best_response_match_a[1]:
            same_distance_responses.append(best_response_match_a[0])
        if min_rd == best_response_match_b[1]:
            same_distance_responses.append(best_response_match_b[0])
        if min_rd == best_response_match_c[1]:
            same_distance_responses.append(best_response_match_c[0])

        if len(same_distance_responses) > 1:
            # print('Same distance')
            glenn_ratings = []
            amber_ratings = []
            final_ratings = []
            for index, row in self.file_content.iterrows():
                q = row['Question'].strip()
                r = row['Response'].strip()
                fr = float(row['Final.rating'].strip().replace(',', '.'))
                gr = float(row['Glenn.s.rating'].strip().replace(',', '.'))
                ar = float(row['Amber.s.rating'].strip().replace(',', '.'))
                if q == question and r in same_distance_responses:
                    glenn_ratings.append(gr)
                    amber_ratings.append(ar)
                    final_ratings.append(fr)

            all_ratings = final_ratings + glenn_ratings + amber_ratings
            all_ratings_counter = Counter(all_ratings)
            return all_ratings_counter.most_common(1)[0][0]

        else:
            if min_rd == best_response_match_a[1]:
                return 1.0
            elif min_rd == best_response_match_b[1]:
                return 0.5
            else:
                return 0.0

