# -*- coding: utf-8 -*-
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktTrainer
import io
import nltk
from nltk.corpus import wordnet as wn
import operator
import pandas as pd
from itertools import product
from gensim.models import Word2Vec
from nltk.corpus import brown
from model_b import text_to_words, ModelB


def modelA(question, answer):
    train_text = io.open("Weightless.txt", mode="r", encoding="utf-8").read()
    trainer = PunktTrainer()
    trainer.INCLUDE_ALL_COLLOCS = True
    trainer.train(train_text)
    tokenizer = PunktSentenceTokenizer(trainer.get_params())
    tokenized_text = tokenizer.tokenize(train_text)

    # NAME = SENTENCE RANKING
    results = dict()
    answer = text_to_words(answer)  #[word for word in answer.split(' ')]
    question = text_to_words(question)  #[word for word in question.split(' ')]
    a_q = answer + question
    for index, sentence in enumerate(tokenized_text):
        countN = 0
        sentence = text_to_words(sentence) #[word for word in sentence.split(' ')]
        for w in a_q:
            if w in sentence:
                countN += 2

            if w == u'Shiranna' and (u'her' in sentence or u'she' in sentence or u'She' in sentence):
                countN += 1

        if countN > 0:
            results[index] = countN

    sorted_results = sorted(results.items(), key=operator.itemgetter(1))
    sorted_results.reverse()
    answer = [t for t in answer if t != u'']
    tagged_answer = nltk.pos_tag(answer)
    max_score = 0.0
    # NAME = SCORING FUNCTION
    for (sentence_index, number_of_matches) in sorted_results[0:5]:
        candidate = text_to_words(tokenized_text[sentence_index]) #.split()
        score = compareA(tagged_answer, candidate)
        if score > max_score:
            max_score = score

    return max_score


def compareA(tagged_answer, candidate):
    count = 0.0
    countAll = 0
    for unic, tag in tagged_answer:
        if tag == 'JJ' or tag == 'VB' or tag == 'VBG' or tag == 'VBP' or tag == 'VBN' or tag == 'VBD' \
                or tag == 'NNS' or tag == 'NN' or tag == 'VBZ' or tag == 'NNP' or tag == 'NNPS' or tag == 'MD':
            countAll += 1
            if str(unic) in candidate:
                count += 1

    if countAll > 0:
        matching_ratio = count / countAll
    else:
        matching_ratio = 0.0

    if matching_ratio > 0.5:
        return 1.0
    elif matching_ratio > 0.3:
        return 0.5
    else:
        return 0.0


def modelC(question, answer, use_wordNet=True, b=None):
    train_text = io.open("Weightless.txt", mode="r", encoding="utf-8").read()
    trainer = PunktTrainer()
    trainer.INCLUDE_ALL_COLLOCS = True
    trainer.train(train_text)
    tokenizer = PunktSentenceTokenizer(trainer.get_params())
    tokenized_text = tokenizer.tokenize(train_text)

    if not use_wordNet:
        tokenized_text_into_words = []
        for index, sentence in enumerate(tokenized_text):
            tokenized_text_into_words.append(nltk.word_tokenize(sentence))

        if b is None:
            b = Word2Vec(brown.sents())


    results = dict()
    answer = text_to_words(answer)  #[word for word in answer.split(' ')]  # if str.lower(str(word)) not in stopwords.words('english')]
    question = text_to_words(question)  #[word for word in question.split(' ')]  # if str.lower(str(word)) not in stopwords.words('english')]
    a_q = answer + question
    a_q = list(set(a_q))

    for index, sentence in enumerate(tokenized_text):
        countN = 0
        sentence = text_to_words(sentence)  #[word for word in sentence.split(' ')]
        for w in a_q:
            if w in sentence:
                countN += 4

            if w == u'Shiranna' and (u'her' in sentence or u'she' in sentence):
                countN += 2

            if use_wordNet:
                synonyms = []

                for syn in wn.synsets(w):
                    for l in syn.lemmas():
                        synonyms.append(l.name())

                for s_word in sentence:
                    if s_word in synonyms:
                        countN += 1
            else:  # use word2vec
                synonyms = []
                most_similar = None
                try:
                    most_similar = b.wv.most_similar(w, topn=10)
                except KeyError as e:
                    print(e)

                if most_similar is not None:
                    for (syn, score) in most_similar:
                        synonyms.append(syn)

                    for s_word in sentence:
                        if s_word in synonyms:
                            countN += 1

        if countN > 0:
            results[index] = countN

    sorted_results = sorted(results.items(), key=operator.itemgetter(1))
    sorted_results.reverse()
    answer = [t for t in answer if t != u'']
    tagged_answer = nltk.pos_tag(answer)
    max_score = 0.0
    # NAME = SCORING FUNCTION
    for (sentence_index, number_of_matches) in sorted_results[0:5]:
        candidate = text_to_words(tokenized_text[sentence_index]) #.split()
        score = compareC(tagged_answer, candidate)
        if score > max_score:
            max_score = score

    return max_score


def compareC(tagged_answer, candidate):
    count = 0.0
    countAll = 0
    for unic, tag in tagged_answer:

        if tag == 'JJ' or tag == 'VB' or tag == 'VBG' or tag == 'VBP' or tag == 'VBN' or tag == 'VBD' \
                or tag == 'NNS' or tag == 'NN' or tag == 'VBZ' or tag == 'NNP' or tag == 'NNPS' or tag == 'MD':
            countAll += 1
            if str(unic) in candidate:
                count += 3
                countAll += 2

            else:
                syns1 = wn.synsets(unic)
                end_syns = False
                for ws in candidate:
                    syns2 = wn.synsets(ws)
                    for sense1, sense2 in product(syns1, syns2):
                        d = wn.wup_similarity(sense1, sense2)
                        if d is not None and d >= 0.7:
                            count += 1
                            end_syns = True
                            break
                    if end_syns:
                        break

    if countAll > 0:
        matching_ratio = count / countAll
    else:
        matching_ratio = 0.0
    if matching_ratio > 0.5 and countAll > 5:
        return 1.0
    elif matching_ratio > 0.3:
        return 0.5
    else:
        return 0.0


def read_train_csv(filename):
    df = pd.read_csv(filename, encoding='utf-8', usecols=[10, 11, 14])
    # print(df)
    return df


if __name__ == '__main__':
    filename = 'Weightless_dataset_train.csv'
    df = read_train_csv(filename)

    #model_b = ModelB(filename)

    # evaluate
    n_all_samples = 0.0
    n_correct = 0.0
    n_ann_zero = 0.0
    n_ann_half = 0.0
    n_ann_one = 0.0
    # what our model scored
    n_final_zero = 0.0
    n_final_half = 0.0
    n_final_one = 0.0
    # confusion matrix
    n_ann_zero_pred_zero = 0.0
    n_ann_zero_pred_half = 0.0
    n_ann_zero_pred_one = 0.0
    n_ann_half_pred_zero = 0.0
    n_ann_half_pred_half = 0.0
    n_ann_half_pred_one = 0.0
    n_ann_one_pred_zero = 0.0
    n_ann_one_pred_half = 0.0
    n_ann_one_pred_one = 0.0

    use_wordNet = True
    print('evaluating.. use_wordNet: {}'.format(use_wordNet))
    b = None
    if not use_wordNet:
        b = Word2Vec(brown.sents())

    for index, row in df.iterrows():
        question = row['Question'].strip()
        answer = row['Response'].strip()
        score = row['Final.rating']

        score = float(score.replace(',', '.'))

        n_all_samples += 1
        final_score = modelC(question, answer)#, use_wordNet=use_wordNet, b=b)

        #final_score = model_b.predict_final_rating(question, answer)

        print('Final score: {}, real score: {}'.format(final_score, score))

        # stats
        if score == 0.0:
            n_ann_zero += 1
            if final_score == 0.0:
                n_final_zero += 1
                n_ann_zero_pred_zero += 1
            elif final_score == 0.5:
                n_final_half += 1
                n_ann_zero_pred_half += 1
            else:
                n_final_one += 1
                n_ann_zero_pred_one += 1

        elif score == 0.5:
            n_ann_half += 1
            if final_score == 0.0:
                n_final_zero += 1
                n_ann_half_pred_zero += 1
            elif final_score == 0.5:
                n_final_half += 1
                n_ann_half_pred_half += 1
            else:
                n_final_one += 1
                n_ann_half_pred_one += 1
        else:
            n_ann_one += 1
            if final_score == 0.0:
                n_final_zero += 1
                n_ann_one_pred_zero += 1
            elif final_score == 0.5:
                n_final_half += 1
                n_ann_one_pred_half += 1
            else:
                n_final_one += 1
                n_ann_one_pred_one += 1

        print('{}/{}'.format(n_ann_zero_pred_zero + n_ann_half_pred_half + n_ann_one_pred_one, n_all_samples))

    print('Accuracy for modelC: {}/{} = {}'.format(n_ann_zero_pred_zero + n_ann_half_pred_half + n_ann_one_pred_one, n_all_samples, (n_ann_zero_pred_zero + n_ann_half_pred_half + n_ann_one_pred_one)/n_all_samples))
    print('scores: {} - {}  - {}, final scores: {} - {} - {}'.format(n_ann_zero, n_ann_half, n_ann_one,
                                                                     n_final_zero, n_final_half, n_final_one))
    print('pred0:\t\t {} {} {}\npred0.5:\t {} {} {}\npred1:\t\t {} {} {}'.format(n_ann_zero_pred_zero, n_ann_half_pred_zero,
                                                                       n_ann_one_pred_zero,
                                                                       n_ann_zero_pred_half, n_ann_half_pred_half,
                                                                       n_ann_one_pred_half,
                                                                       n_ann_zero_pred_one, n_ann_half_pred_one,
                                                                       n_ann_one_pred_one))
